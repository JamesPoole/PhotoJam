<style>
  img {
    max-width: 300px;
    margin-top: 50px;
    margin-bottom: 50px;
    margin-left: 40px;
    transform: rotate(270deg);
    border: 2px solid darkgrey;
  }
</style>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your Photos</div>
                <div class="panel-body">
                    @foreach ($photos as $photo)
                        <a href="/home/photos/{{ $photo->face_name }}"><img src="{{ url($photo->path) }}"/></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
