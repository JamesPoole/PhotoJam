<style>
  img {
    max-width: 300px;
    margin-top: 50px;
    margin-bottom: 50px;
    margin-left: 40px;
    transform: rotate(270deg);
    border: 2px solid darkgrey;
  }
</style>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if ($photos[0]->face_name === "unknown")
                    <div class="panel-heading">Unknown Faces (Tag each face to improve the predictions)</div>
                @else
                    <div class="panel-heading">{{ $photos[0]->face_name }}</div>
                @endif
                <div class="panel-body">
                    @foreach ($photos as $photo)
                        <a href="/home/photos/face/{{ $photo->file_name }}"><img src="{{ url($photo->path) }}"/></a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
