<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'PhotoController@index');

Route::get('/home/photos', 'PhotoController@index');

Route::get('/home/photos/{face_id}', 'PhotoController@show_face');

Route::get('/home/photos/face/{file_name}', 'PhotoController@show_image');

Route::post('/home/photos/{file_name}', 'PhotoController@update');
