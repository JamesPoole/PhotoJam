import os, sys, shutil
import time
import json
import argparse
from subprocess import *

from facenet import load_model, load_data

from align.align_dataset_mtcnn import main as align
import tensorflow as tf
from sklearn import svm
import numpy as np

import MySQLdb
import pickle

####################################
#Paths
####################################
photos_path = "/root/Dropbox/Camera Uploads/"
model_path = "/root/20170512-110547.pb"
new_photos = "/root/facial_recognition/new_photos/"
aligned_photos = "aligned_photos/"

###################################
#Database Setup
###################################
try:
    db = MySQLdb.connect(host="photodata.cexbzufp7beq.eu-west-1.rds.amazonaws.com",
            user=sys.argv[1],
            passwd=sys.argv[2],
            db=sys.argv[3])

    cursor = db.cursor()
#    cursor.execute("""DELETE FROM photos""")
#    db.commit()

except:
    print("Invalid Database information. ARGS: jamphotos.py <db_username> <db_pass> <db_name>")

###################################
#Argparser setup for compatibility with facenet align
###################################
parser = argparse.ArgumentParser()
parser.add_argument('--input_dir')
parser.add_argument('--output_dir')
parser.add_argument('--image-size', type=int)
parser.add_argument('--margin', type=int)
parser.add_argument('--random-order')
parser.add_argument('--gpu-memory-fraction', type=float)
parser.add_argument('--has_classes')
parser.add_argument('--no-classes')
align_args = parser.parse_args(['--input_dir', '/root/facial_recognition/new_photos/', '--output_dir', '/root/facial_recognition/aligned_photos/'
 ,'--image-size', '182', '--margin', '44', '--random-order', 'True', '--gpu-memory-fraction', '1.0', '--no-classes', 'True', '--has_classes', 'False' ])

##################################
#Get facial embedding for new face
##################################
def get_embedding(new_face_path):
    """
    get_embedding -     function to take in the path of an aligned photo and get
                        the facial embedding from that photo

    args -  new_face_path   path to aligned photo

    returns result      embedding for face
    """
    with tf.Graph().as_default():
        with tf.Session() as sess:

            load_model(model_path)

            # Get input and output tensors
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

            #load photo
            image_paths = [new_face_path]
            images = load_data(image_paths=image_paths, do_random_crop=False, do_random_flip=False, image_size=160, do_prewhiten=True)
            feed_dict = {images_placeholder: images, phase_train_placeholder: False}

            #create an embedding array
            emb_array = sess.run(embeddings, feed_dict=feed_dict)

            return emb_array[0]

#################################
#Predict the newest face
#################################
def predict_face(model, new_embedding):
    print("Predicting face in image...")
    emb_list = [new_embedding]
    predict_response = model.predict_proba(emb_list)
    best_class_indices = np.argmax(predict_response, axis=1)

    return (best_class_indices[0]+1)

#################################
#Re train svm with newest data
#################################
def train_svm(data, labels_int):
    clf = svm.SVC(kernel="linear", probability=True)

    print(data)
    print(labels_int)
    print("Training and becoming more self aware...")
    clf.fit(data, labels_int)
    print("New model trained...")

    return clf

#################################
#Parse the database for latest data
#################################
def parse_database():
    data = []
    labels = []
    labels_int = []
    labels_dict = {}
    labels_dict_reverse = {}

    cursor.execute("""SELECT face_id, face_name, embedding FROM photos""")

    result = cursor.fetchall()

    seen_faces = []

    new_faces = []
    new_embeddings = []
    for i in result:
        emb_string = i[2]
        emb = pickle.loads(emb_string)

        #face has been given a new name but still has id of 0
        if i[0] == 0 and i[1] != "unknown":
            #give new face_id
            new_faces.append(i[1])
            new_embeddings.append(emb)

        #face has not yet been given a label, discard data
        elif i[0] == 0 and i[1] == "unknown":
            pass

        #face is known and has a label
        elif i[0] != 0:
            labels_int.append(i[0])
            data.append(emb)
            labels_dict.update({i[0]: i[1]})
            labels_dict_reverse.update({i[1]: i[0]})

        #update label reference dictionary
        if i[1] not in seen_faces and i[0] != 0:
            labels_dict.update({i[0]: i[1]})
            labels_dict_reverse.update({i[1]: i[0]})
            seen_faces.append(i[1])

    #Check for the next available face_id for a new face
    for i in range(0, len(new_faces)):
        if new_faces[i] in labels_dict.values():
            data.append(new_embeddings[i])
            labels_int.append(labels_dict_reverse[new_faces[i]])
            continue

        for x in range(1, 200):
            if x in labels_dict:
                continue
            else:
                cursor.execute("""UPDATE photos SET face_id=%s WHERE face_name=%s""",(x, new_faces[i]))
                db.commit()
                data.append(new_embeddings[i])
                labels_int.append(x)
                labels_dict.update({x: new_faces[i]})
                labels_dict_reverse.update({new_faces[i]: x})
                break

    return data, labels_int, labels_dict

#################################
#Main Loop
#################################
def main():
    print("Starting AI backend...")
    print("Waiting for new photo...")
    old_files = []
    try:
        with open('processed_faces', 'rb') as processed_faces:
            old_files = pickle.load(processed_faces)
    except:
        with open('processed_faces', 'wb') as processed_faces:
            pickle.dump(old_files, processed_faces)

    print(old_files)

    model = None
    #Keep checking for new files into the Dropbox Camera folder
    while(1):
            time.sleep(.2)
            new_files = os.listdir( photos_path )
            if new_files != old_files:
                    for file in new_files:
                            if file not in old_files:
                                    print("New photo found...")
                                    old_files.append(file)
                                    path = os.path.join(photos_path, file)
                                    shutil.copy(path, new_photos)
                                    new_photos_path = os.path.join(new_photos, file)

                                    #Use mtcnn to crop the face
                                    align(align_args)

                                    #Use FaceNet to extract the embedding of a face
                                    print("Extracting embedding of face...")
                                    aligned_face_path = os.path.join(aligned_photos, file)
                                    aligned_face_path = aligned_face_path.replace(".jpg", ".png")
                                    try:
                                        new_face_embedding = get_embedding(aligned_face_path)
                                    except:
                                        print("aligning failed...trying to get embedding from raw image")
                                        new_face_embedding = get_embedding(path)
                                    embedding_binary = pickle.dumps(new_face_embedding)

                                    #retrieve the latest data from the database
                                    data, labels_int, labels_dict = parse_database()

                                    #not enough data to run training
                                    if len(data) < 3:
                                        #Zero if the label of an unkown face, to be sent to the
                                        #gui to get a face name
                                        default_face_id = 0
                                        default_face_name = "unknown"
                                        cursor.execute("""INSERT INTO photos VALUES(%s,%s,%s,%s,%s)""", (path, file, default_face_id, default_face_name, embedding_binary))
                                        db.commit()
                                        print("Photo added to database for tagging...")

                                    #run first training
                                    elif len(data) >= 3 and model == None and len(labels_dict) = 1:
                                        print("Creating first model...")
                                        model = train_svm(data, labels_int)
                                        print(labels_dict)
                                        predicted_label_int = predict_face(model, new_face_embedding)
                                        print(predicted_label_int)
                                        predicted_label_str = labels_dict[predicted_label_int]

                                        cursor.execute("""INSERT INTO photos VALUES (%s,%s,%s,%s,%s)""", (path, file, predicted_label_int, predicted_label_str, embedding_binary))
                                        db.commit()

                                        print("Face information updated...")

                                    #predict face and improve model
                                    elif len(data) > 3 and model != None:
                                        predicted_label_int = predict_face(model, new_face_embedding)

                                        #TODO if successful prediction
                                        if True:
                                            predicted_label_str = labels_dict[predicted_label_int]

                                            cursor.execute("""INSERT INTO photos VALUES (%s,%s,%s,%s,%s)""", (path, file, predicted_label_int, predicted_label_str, embedding_binary))
                                            db.commit()

                                            print("Face information updated...")

                                    os.remove(new_photos_path)
                                    with open('processed_faces', 'wb') as processed_faces:
                                        pickle.dump(old_files, processed_faces)
                                    print("Waiting for new photo...")

main()
